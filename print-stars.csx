string Times(string s, int n)
{
	string result = "";
	for(int i = 0; i < n; ++i)
		result += s;
	return result;
}

int n = 5;

// for(int i = 1; i <= 5; ++i)
// 	Console.WriteLine( Times("*", i) );


// for(int i = 1; i <= n; ++i)
// 	Console.WriteLine( Times(" ", n - i) + Times("*", i) );

//     *
//    * *
//   * * *
//  * * * *
// * * * * *

for(int i = 1; i <= 5; ++i)
	Console.WriteLine( Times(" ", n - i) + Times("* ", i) );
