void Times(int n, Action action)
{
	for(var i = 0; i < n; ++i)
		action();
}

 List<int> Range(int start, int stop)
 {
 	var list = new List<int>();
 	for(int i = start; i < stop; ++i)
 		list.Add(i);
 	return list;
 }