// IsDateValid
// -> IsYearValid
// -> IsMonthValid
// -> IsDayValid
//    -> DaysInMonth
//    	  -> IsLeapYear

bool IsLeapYear(int year)
{
	return year % 400 == 0 ||
		(year % 100 != 0 && year % 4 == 0);
}

int DaysInMonth(int month, int year)
{
	if(month == 4 || month == 6 || month == 9 || month == 11)
		return 30;
	if(month == 2)
		return IsLeapYear(year) ? 29 : 28;
	return 31;
}

bool IsYearValid(int year)
{
	const int GREGORIAN_START_YEAR = 1582;
	return year > GREGORIAN_START_YEAR;
}

bool IsMonthValid(int month)
{
	return month >= 1 && month <= 12;
}

bool IsDayValid(int day, int month, int year)
{
	return day >= 1 && day <= DaysInMonth(month, year);
}

bool IsDateValid(int day, int month, int year)
{
	return IsYearValid(year) && IsMonthValid(month) && IsDayValid(day, month, year);
}
