List<string> Lines(StreamReader reader)
{
	var lines = new List<string>();
	string line;
	while( (line = reader.ReadLine()) != null)
		lines.Add(line);
	return lines;
}

string Chars(StreamReader reader)
{
	string chars = "";
	int ch;
	while( (ch = reader.Read()) != -1)
		chars += (char)ch;
	return chars;
}

// FIX : Remove empty strings
List<string> Words(StreamReader reader)
{
	var words = new List<string>();
	foreach(var line in Lines(reader))
		words.AddRange(line.Split());
	return words;
}