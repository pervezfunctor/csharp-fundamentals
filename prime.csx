bool IsPrime(int n)
{
	for(int i = 2; i < n; ++i)
		if(n % i == 0)
			return false;			
	return true;
}

void PrintPrimes(int start, int stop)
{
	for(int i = start; i < stop; ++i)
		if(IsPrime(i))
			Console.Write(i + " ");
	Console.WriteLine();	
}
