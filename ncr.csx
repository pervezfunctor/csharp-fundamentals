long Factorial(long n) 
{
	long fact = 1;
	for(long i = 2; i <= n; ++i)
		fact *= i;
	return fact;
}

long Ncr(long n, long r)
{
	return Factorial(n) / ( Factorial(r) * Factorial(n - r));
}
