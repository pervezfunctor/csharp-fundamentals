void PrintList(List<int> list)
{
	foreach(var e in list)
		Console.Write(e + " ");
	Console.WriteLine();	
}

string Join(List<int> list, string delimiter)
{
	string s = "";
	foreach(var e in list)
		s += e + delimiter;
	return s;
}

int Sum(List<int> list)
{
	var sum = 0;
	foreach(var e in list)
		sum += e;
	return sum;
}

int Max(int x, int y)
{
	return x > y ? x : y;
}

int Max(List<int> list)
{
	var max = list[0];
	for(var i = 1; i < list.Count; ++i)
		max = Max(max, list[i]);		
	return max;
}

List<int> Range(int start, int stop)
{
	var list = new List<int>();
	for(int i = start; i < stop; ++i)
		list.Add(i);
	return list;
}

List<int> Range(int stop)
{
	return Range(0, stop);
}

int IndexOf(List<int> list, int e, int pos = 0)
{
	for(int i = pos; i < list.Count; ++i)
		if(list[i] == e)
			return i;
	return -1;
}

List<int> AllOcccurences(List<int> list, int e)
{
	
	var indices = new List<int>();
	for(var pos = 0; (pos = IndexOf(list, e, pos)) != -1; ++pos)	
		indices.Add(pos);
	return indices;
}
